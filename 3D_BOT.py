# -*- coding: utf-8 -*-

# Рабочий релиз @Machinecontrol_bot V 1.0

# Импортировали модуль pandas
import pandas as pd

# Импортировали модуль SQLite
import sqlite3 as sl


# Импортируем Телеграм бота
import telebot

bot = telebot.TeleBot("TOKEN was here")

# Подключаем базу данных и курсор
con = sl.connect("3D.db", check_same_thread = False)
cur = con.cursor()

# Отклик на запуск бота
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
	bot.reply_to(message, "Введите ГосНомер (0000 АА 00): ")
	
# Настраиваем бота на прием информации от пользователя
@bot.message_handler(content_types=['text'])

# Определяем функцию, которая производит поиск и вывод данных
def echo_all(message):
    m_ID = message.text
    cur.execute ("SELECT * FROM machinecontrol WHERE ГосНомер = ?",(m_ID,))
    row = cur.fetchall()
    s = str(row)
    print (s)
    bot.reply_to(message, s)

bot.infinity_polling()
